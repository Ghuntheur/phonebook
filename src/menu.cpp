#include "menu.hpp"

/**
 * @brief Print to console the main (starting) menu
 * 
 */
void Menu::printMainMenu() {
  std::cout << "0 -> Quitter" << std::endl;
  std::cout << "1 -> Parcourir le répertoire" << std::endl;
  std::cout << "2 -> Ajouter un contact" << std::endl;
  std::cout << "3 -> Exporter les données" << std::endl;
  std::cout << "4 -> Importer des données\n" << std::endl;
}

/**
 * @brief Print a menu to demand who (contact) print
 * 
 */
void Menu::printChooseContactMenu(int size) {
  std::cout << "\n0 -> Retour" << std::endl;
  std::cout << "1 à " << size << " -> Détails d'un contact\n" << std::endl;
}

void Menu::printContactActionsMenu() {
  std::cout << "\n0 -> Retour" << std::endl;
  std::cout << "1 -> Éditer l'identité" << std::endl;
  std::cout << "2 -> Éditer l'adresse" << std::endl;
  std::cout << "3 -> Supprimer\n" << std::endl;
}

/**
 * @brief User input while number not correspond to action
 * 
 * @param start 
 * @param end 
 * @return int 
 */
int Menu::menuChoice(int start, int end) {
  int output;
  do {
    output = Menu::_userChoice();
  } while(output < start || output > end+1);
  return output;
}

/**
 * @brief User input to know action
 * 
 * @return int 
 */
int Menu::_userChoice() {
  std::cout << "Saisir un nombre : ";
  int output;
  std::cin >> output;
  Menu::waitForInt();
  return output;
}

/**
 * @brief Fix console error
 * 
 */
void Menu::waitForInt() {
  std::cin.clear();
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

/**
 * @brief Clear the console
 */
void Menu::clear() {
  system("clear");
}