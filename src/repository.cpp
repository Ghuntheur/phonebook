#include "repository.hpp"

/**
 * @brief Repository constructor
 * 
 */
Repository::Repository(int _currentIndex)
  : currentIndex(_currentIndex) { }

/**
 * @brief 
 * 
 * @param index (int) -> new active index -> choosen contact 
 */
void Repository::setCurrentIndex(int index) {
  this->currentIndex = index;
}

/**
 * @brief Run phonebook and demand action
 * 
 */
void Repository::run() {
  // Read, add, import or export
  Menu::printMainMenu();  
  int output = Menu::menuChoice(MAIN_BACK, EXPORT_DATA);
  Repository::_dispatchMainMenuActions(output);
}

/**
 * @brief Print the phonebook
 * 
 */
void Repository::read() {
  int size = this->list.size();

  // Si répertoire vide
  if (size == 0) {
    std::cout << "----- RÉPERTOIRE VIDE ! -----" << std::endl << std::endl;
    Repository::run();
  } else {
    std::cout << "----- RÉPERTOIRE -----" << std::endl;
    for(int i; i<size; i++) {
      std::cout << i+1 << "] " << this->list[i].getResume() << std::endl;
    }
  }  
}

/**
 * @brief Return if phonebook is enmpty or not
 * 
 * @return true -> phonebook not empty 
 * @return false -> phonebook empty
 */
bool Repository::notEmpty() {
  return this->list.size() > 0;
}

/**
 * @brief Delete a contact from the phonebook
 * 
 * @param index active contact
 */
void Repository::deleteContact(int index) {
  this->list.erase(this->list.begin()+index);
}

/**
 * @brief Export the phonebook to csv file
 * 
 */
void Repository::exportData() {
  std::ofstream file;
  file.open("export.csv");

  if (!Repository::notEmpty()) {  
    std::cout << "Aucune donnée à exporter" << std::endl;
    return;
  }

  // ecriture
  // EN TETE
  file << "Firstname, Lastname, Age, PhoneNumber, Address Number, Street, City, Zip Code\n";
  // Contact
  int size = this->list.size();
  for (int i=0; i<size; i++) {
    Contact &c = this->list[i];
    Address &a = c.getAddress();
    file << c.getFirstname() << "," << c.getLastname() << "," << c.getAge() << "," << c.getPhoneNumber();
    if (a.exists()) {
      file << "," << a.getNumber() << "," << a.getStreet() << "," << a.getCity() << "," << a.getZipCode() << "\n";
    } else {
      file << "\n";
    }
  }
  file.close();
}

void Repository::importData() {
  std::ifstream file("import.csv");
  

  std::string firstname, lastname, phoneNumber, street, city;
  int age, number, zipCode;

  std::string line;
  while (!file.eof()) {
    // Identity
    std::getline(file, line, ',');
    firstname = line;
    std::getline(file, line, ',');
    lastname = line;
    std::getline(file, line, ',');
    age = std::atoi(line.c_str());
    std::getline(file, line, ',');
    phoneNumber = line;

    // Address
    std::getline(file, line, ',');
    number = std::atoi(line.c_str());
    std::getline(file, line, ',');
    street = line;
    std::getline(file, line, ',');
    city = line;
    std::getline(file, line, ',');
    zipCode = std::atoi(line.c_str());
   
    Address address(number, street, city, zipCode);
    Contact contact(firstname, lastname, phoneNumber, age, address);
    this->list.push_back(contact);
  }
  file.close();
}

/**
 * @brief Dispatch main menu actions
 * PRIVATE
 * @param action -> user input
 */
void Repository::_dispatchMainMenuActions(int action) {
  switch(action) {
    // Exit
    case MAIN_BACK:
      Menu::clear();
      exit(EXIT_FAILURE);
      break;

    //Read
    case READ: {
      Menu::clear();
      Repository::read();
      if (Repository::notEmpty()) {
        int size = this->list.size();
        Menu::printChooseContactMenu(size);
        Repository::_dispatchChooseContactMenuActions(Menu::menuChoice(0, size));
      }
      break;
    }

    // Add Contact
    case ADD_CONTACT: {
      Menu::clear();
      Contact person = Contact::createContact();
      this->list.push_back(person);
      person.printNewContact();
      Repository::run();
      break;
    }

    // Export data
    case EXPORT_DATA:
      Repository::exportData();
      Repository::run();
      break;

    //   // Import data
    case IMPORT_DATA:
      Repository::importData();
      std::cout << "Données importées\n" << std::endl;
      Repository::run();
      break;

    default:
      break;
    
  }
}

/**
 * @brief Dispatch actions when user choose a contact that exists or not
 * PRIVATE
 * @param action -> user input
 */
void Repository::_dispatchChooseContactMenuActions(int action) {
  Menu::clear();
  int size = this->list.size();
  // if back
  if (action == 0) {
    Repository::run();    
  } else if (action > 0 && action <= size) {
    int index = action-1;
    Repository::setCurrentIndex(index);

    Contact &contact = this->list[this->currentIndex];
    contact.printDetails();

    Menu::printContactActionsMenu();
    Repository::_dispatchContactMenuActions(Menu::menuChoice(CONTACT_BACK, DELETE));
  } else {
    Repository::_dispatchMainMenuActions(READ);
  }
}

/**
 * @brief Dispatch action to do with a contact (edit, delete...)
 * 
 * @param action -> user input
 */
void Repository::_dispatchContactMenuActions(int action) {
  Menu::clear();
  Contact &contact = this->list[this->currentIndex];
  switch (action) {
    // Back
    case CONTACT_BACK:
      Repository::_dispatchMainMenuActions(READ);
      break;

    // Edit identity 
    case SET_IDENTITY:
      contact.setIdentity();
      contact.printUpdateContact();
      Repository::_dispatchChooseContactMenuActions(this->currentIndex+1);
      break;

    // Edit address
    case SET_ADDRESS: {
      Address &address = contact.getAddress();
      address.setAddress();
      address.printUpdateAddress();
      Repository::_dispatchChooseContactMenuActions(this->currentIndex+1);
      break;
    }

    // Delete
    case DELETE:
      Repository::deleteContact(this->currentIndex);
      Repository::run();
      break;

    default:
      break;
  }
}