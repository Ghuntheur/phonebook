#include <iostream>

// INCLUDE HEADER FILES
#include "enum.hpp"
#include "menu.hpp"
#include "repository.hpp"
#include "contact.hpp"
#include "address.hpp"


int main() {

  Menu::clear();

  Repository phonebook;
  phonebook.run();
  
  return EXIT_SUCCESS;
}
