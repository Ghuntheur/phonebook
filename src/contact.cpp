#include "contact.hpp"

/**
 * @brief Construct new contact
 * 
 * @param _firstname 
 * @param _lastname 
 * @param _phoneNumber 
 * @param _age 
 * @param _address 
 */
Contact::Contact(std::string _firstname,
                 std::string _lastname,
                 std::string _phoneNumber,
                 int _age,
                 Address _address) 
  : firstname(_firstname), lastname(_lastname), phoneNumber(_phoneNumber), age(_age), address(_address) { }

/**
 * @brief Print new contact (resume)
 * 
 */
void Contact::printNewContact() {
  Menu::clear();
  std::cout << "Nouveau contact : " << Contact::getResume() << "\n" << std::endl;
}

/**
 * @brief Print contact after identity update
 * 
 */
void Contact::printUpdateContact() {
  Menu::clear();
  std::cout << "Contact modifié : " << Contact::getResume() << "\n" << std::endl;
}

/**
 * @brief Print details of a contact (identity and address)
 * 
 */
void Contact::printDetails() {
  Contact::printIdentity();
  Contact::printAddress();
}

/**
 * @brief print Identity of a contact
 * ----- IDENTITY -----
 * Prenom Nom - XX ans
 * Tel : 0123456789
 * 
 */
void Contact::printIdentity() {
  std::cout << "----- IDENTITÉ -----" << std::endl;
  std::cout << this->firstname << " " << this->lastname << " - " << this->age << " ans" << std::endl;
  std::cout << "Tel : " << this->phoneNumber << std::endl;
}

/**
 * @brief Print Address of a contact
 * ----- ADDRESSE -----
 * 4 PlaceDeLaRépublique
 * 87000 Limoges
 */
void Contact::printAddress() {
  Address &address = this->getAddress();
  if (address.exists()) {
    std::cout << "----- ADRESSE -----" << std::endl;
    address.printAddress();
  }
}

/**
 * @brief Return a new Contact after user choice.
 * Some simple verifications
 * 
 * Static 
 * @return Contact 
 */
Contact Contact::createContact() {
  std::string firstname, lastname, phoneNumber;
  int age;
  Address address;

  std::cout << "Prénom : ";
  std::cin >> firstname;

  std::cout << "Nom : ";
  std::cin >> lastname;

  do {
    std::cout << "Âge : ";
    std::cin >> age;
    Menu::waitForInt();
  } while (age <=0 || age > 130);

  do {
    std::cout << "Téléphone : ";
    std::cin >> phoneNumber;
  } while (phoneNumber.length() != 10);

  return Contact(firstname, lastname, phoneNumber, age, address);
}

// Getter 

Address& Contact::getAddress() {
  return this->address;
}
std::string Contact::getResume() const {
  return this->firstname + " " + this->lastname + " - " + std::to_string(this->age) + " ans - " + this->phoneNumber;
}
std::string Contact::getFirstname() const {
  return this->firstname;
}
std::string Contact::getLastname() const {
  return this->lastname;
}
std::string Contact::getPhoneNumber() const {
  return this->phoneNumber;
}
int Contact::getAge() const {
  return this->age;
}

// Setter 

void Contact::setIdentity() {
  std::cout << "Nouveau prénom : ";
  std:: cin >> this->firstname;

  std::cout << "Nouveau nom : ";
  std::cin >> this->lastname;

  do {
    std::cout << "Nouvel âge : ";
    std::cin >> this->age;
    Menu::waitForInt();    
  } while (this->age <=0 || this->age > 130);

  do {
    std::cout << "Nouveau tel : ";
    std::cin >> this->phoneNumber;
  } while (this->phoneNumber.length() != 10);
}