#include "address.hpp"

/**
 * @brief Address constructor
 * 
 * @param _number 
 * @param _street 
 * @param _city 
 * @param _zipCode 
 */
Address::Address(int _number,
                 std::string _street,
                 std::string _city,
                 int _zipCode) 
  : number(_number), street(_street), city(_city), zipCode(_zipCode) { }

/**
 * @brief Print address of a contact
 * 
 */
void Address::printAddress() {
  std::cout << this->number << " " << this->street << std::endl;
  std::cout << this->zipCode << " " << this->street << std::endl;
}

/**
 * @brief Print address of a contact after update
 * 
 */
void Address::printUpdateAddress() {
  std::cout << "Adresse modifiée : " << this->getResume() << std::endl;
}

/**
 * @brief Return if address exists
 * 
 * @return true -> exists
 * @return false -> does not exist
 */
bool Address::exists() {
  return this->number != 0;
}

// Getter
std::string Address::getResume() const {
  return std::to_string(this->number) + " " + this->street + " - " + std::to_string(this->zipCode) + " " + this->city;
}
std::string Address::getStreet() const {
  return this->street;
}
std::string Address::getCity() const {
  return this->city;
}
int Address::getNumber() const {
  return this->number;
}
int Address::getZipCode() const {
  return this->zipCode;
}


// Setter

void Address::setAddress() {
  do {
    std::cout << "N° de rue : ";
    std::cin >> this->number;
  } while (this->number <= 0);

  std::cout << "Rue : ";
  std::cin >> this->street;

  std::cout << "Ville : ";
  std::cin >> this->city;

  do {
    std::cout << "Code postal : ";
    std::cin >> this->zipCode;
  } while (this->zipCode < 10000 || this->zipCode > 99999);
  
}