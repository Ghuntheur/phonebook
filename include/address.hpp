#ifndef ADDRESS_HPP_
#define ADDRESS_HPP_

#include <iostream>
#include <string>

class Address {

private:
  int number;
  std::string street;
  std::string city; 
  int zipCode;

public:
  Address(int _number = 0,
          std::string _street = "",
          std::string _city = "",
          int _zipCode = 0);
  void printAddress();
  void printUpdateAddress();
  
  bool exists();

  // Getter
  std::string getResume() const;
  std::string getStreet() const;
  std::string getCity() const;
  int getNumber() const;
  int getZipCode() const;

  // Setter
  void setAddress();

};

#endif