#ifndef REPOSITORY_HPP_
#define REPOSITORY_HPP_

#include <iostream>
#include <fstream>
#include <fstream>
#include <vector>

#include "enum.hpp"
#include "menu.hpp"
#include "contact.hpp"

class Repository {

private:
  std::vector<Contact> list;
  int currentIndex;

public:
  Repository(int _currentIndex = -1);

  void run();
  void read();
  bool notEmpty();
  void deleteContact(int index);

  void exportData();
  void importData();

  // setter
  void setCurrentIndex(int index);

private:
  void _dispatchMainMenuActions(int action);
  void _dispatchChooseContactMenuActions(int action);
  void _dispatchContactMenuActions(int action);
};

#endif