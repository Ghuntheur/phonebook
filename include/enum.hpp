#ifndef ENUM_HPP_
#define ENUM_HPP_

enum MainActions {
  MAIN_BACK,
  READ,
  ADD_CONTACT,
  EXPORT_DATA,
  IMPORT_DATA
};

enum ContactActions {
  CONTACT_BACK,
  SET_IDENTITY,
  SET_ADDRESS,
  DELETE
};

#endif