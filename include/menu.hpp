#ifndef MENU_HPP_
#define MENU_HPP_

#include <iostream>
#include <limits>

class Menu {
    
public:
  // Output menu
  static void printMainMenu();
  static void printChooseContactMenu(int size);  // choose who
  static void printContactActionsMenu(); // read more, update identity, update address
  
  static int menuChoice(int start, int end);
  static void waitForInt();
  
  static void clear();
  
private:
  static int _userChoice();

};

#endif