#ifndef CONTACT_HPP_
#define CONTACT_HPP_

#include <iostream>
#include <string>

#include "menu.hpp"
#include "address.hpp"

class Contact {

private:
  std::string firstname;
  std::string lastname;
  std::string phoneNumber;
  int age;
  Address address;

public:
  Contact(std::string _firstname, std::string _lastname, std::string _phoneNumber, int _age, Address _address);

  void printNewContact();  
  void printUpdateContact();
  void printDetails();
  void printIdentity();
  void printAddress();
  
  static Contact createContact();
  
  // Getter
  std::string getResume() const;
  Address& getAddress();

  std::string getFirstname() const;
  std::string getLastname() const;
  std::string getPhoneNumber() const;
  int getAge() const;

  // Setter
  void setIdentity();
};

#endif